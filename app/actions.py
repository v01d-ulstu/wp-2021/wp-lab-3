import datetime

import werkzeug.security
import flask
import flask_login
import sqlalchemy.exc

import models


def str_field_is_true(field: str, acceptable=("1", "true", "on")) -> bool:
    return field.strip().lower() in acceptable


def render_template_with_defaults(*args, **kwargs):
    return flask.render_template(
        *args,
        current_user=flask_login.current_user,
        time_in_header=datetime.datetime.utcnow(),
        **kwargs,
    )


actions = flask.Blueprint("actions", __name__)


@actions.route("/")
def action_index():
    posts = models.db.query(models.Post).order_by(models.Post.publish_date.desc()).all()

    return render_template_with_defaults(
        "index.html",
        posts=posts,
        len=len,
    )


@actions.route("/post/<int:post_id>")
def action_view_post(post_id: int):
    post = models.db.query(models.Post).get(post_id)
    if (
        post is None
        or post.hidden
        and (
            flask_login.current_user.is_anonymous
            or not flask_login.current_user.is_admin
        )
    ):
        flask.abort(404, "Post not found")

    liked_by_user = (
        flask_login.current_user.is_authenticated
        and models.Like.find_user_like(flask_login.current_user.id, post_id) is not None
    )

    return render_template_with_defaults(
        "article_page.html",
        post=post,
        liked_by_user=liked_by_user,
        len=len,
    )


@actions.route("/register", methods=["GET", "POST"])
def action_register():
    if flask.request.method == "GET":
        return render_template_with_defaults("register.html")

    try:
        login = flask.request.form["login"].strip()
        password1 = flask.request.form["password1"].strip()
        password2 = flask.request.form["password2"].strip()
    except KeyError:
        flask.abort(400, "No login or passwords")

    if not login or not password1 or not password2:
        flask.abort(400, "Empty login or passwords")

    if not login.isalnum():
        flask.abort(400, "Login must contain letters and digits only")

    user = models.User(
        login=login, password=werkzeug.security.generate_password_hash(password1)
    )
    models.db.session.add(user)

    try:
        models.db.session.commit()
    except sqlalchemy.exc.IntegrityError:
        flask.abort(403, "Login is not unique")

    flask_login.login_user(user, remember=True)

    return flask.redirect("/")


@actions.route("/login", methods=["GET", "POST"])
def action_login():
    if flask.request.method == "GET":
        return render_template_with_defaults("login.html")

    try:
        login = flask.request.form["login"].strip()
        password = flask.request.form["password"].strip()
        remember = str_field_is_true(flask.request.form.get("remember", "false"))
    except KeyError:
        flask.abort(400, "No login or password or remember option")

    try:
        user = models.db.query(models.User).filter(models.User.login == login).one()
    except sqlalchemy.exc.NoResultFound:
        flask.abort(404, "User not found")

    if werkzeug.security.check_password_hash(user.password, password):
        flask_login.login_user(user, remember)
    else:
        flask.abort(403, "Wrong password")

    return flask.redirect("/")


@actions.route("/log-out")
@flask_login.login_required
def action_signout():
    flask_login.logout_user()
    return flask.redirect("/login")


@actions.route("/add-post", methods=["GET", "POST"])
@flask_login.login_required
def action_add_post():
    if not flask_login.current_user.is_admin:
        flask.abort(403, "Admin access required")

    if flask.request.method == "GET":
        return render_template_with_defaults(
            "article_creation_page.html", editing=False
        )

    try:
        title = flask.request.form["title"].strip()
        text = flask.request.form["text"].strip()
        images = flask.request.form["images"].strip()
    except KeyError:
        flask.abort(400, "No text or title or images")

    if not title or not text or not images:
        flask.abort(400, "Empty title or text or images")

    post = models.Post(
        title=title,
        text=text,
        publish_date=datetime.datetime.utcnow(),
    )
    post.images.extend(models.Image(url=image_url) for image_url in images.split("\n"))
    flask_login.current_user.posts.append(post)

    models.db.session.commit()

    return flask.redirect("/")


@actions.route("/post/<int:post_id>/edit", methods=["GET", "POST"])
@flask_login.login_required
def action_edit_post(post_id):
    if not flask_login.current_user.is_admin:
        flask.abort(403, "Admin access required")

    post = models.db.query(models.Post).get(post_id)
    if post is None:
        flask.abort(404, "Post not found")

    if flask.request.method == "GET":
        return render_template_with_defaults(
            "article_creation_page.html", post=post, editing=True
        )

    try:
        title = flask.request.form["title"].strip()
        text = flask.request.form["text"].strip()
        images = flask.request.form["images"].strip()
    except KeyError:
        flask.abort(400, "No text or title or images")

    if not title or not text or not images:
        flask.abort(400, "Empty title or text or images")

    post.title = title
    post.text = text

    for image in post.images:
        models.db.delete(image)

    post.images.extend(models.Image(url=image_url) for image_url in images.split("\n"))
    models.db.commit()

    return flask.redirect(f"/post/{post.id}")


@actions.route("/toggle-like-post")
@flask_login.login_required
def action_toggle_like_post():
    try:
        post_id = int(flask.request.args["post_id"])
    except (KeyError, ValueError):
        flask.abort(400, "post_id is not provided or not int")

    post = models.db.query(models.Post).get(post_id)
    if post is None:
        flask.abort(404, "Post not found")

    current_user_id = flask_login.current_user.id
    like = models.Like.find_user_like(current_user_id, post_id)

    if like is None:
        models.db.add(models.Like(post_id=post_id, user_id=current_user_id))
    else:
        models.db.delete(like)

    models.db.commit()

    return flask.redirect(f"/post/{post_id}")


@actions.route("/toggle-post-visibility")
@flask_login.login_required
def action_toggle_post_visibility():
    if not flask_login.current_user.is_admin:
        flask.abort(403, "Admin access required")

    try:
        post_id = int(flask.request.args["post_id"])
    except (KeyError, ValueError):
        flask.abort(400, "post_id is not provided or not int")

    post = models.db.query(models.Post).get(post_id)
    if post is None:
        flask.abort(404, "Post not found")

    post.hidden = not post.hidden

    models.db.commit()

    return flask.redirect(f"/post/{post_id}")
