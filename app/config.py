import os
import dotenv


dotenv.load_dotenv()


TEMPLATE_FOLDER = os.path.abspath("templates")
STATIC_FOLDER = os.path.abspath("static")
DATABASE_PATH = os.path.abspath("lab3.sqlite3")
REMEMBER_COOKIE_DURATION = 365  # days
FLASK_SECRET_KEY = os.environ["FLASK_SECRET_KEY"]
