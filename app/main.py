import datetime

import flask

import config
import actions
import login


app = flask.Flask(
    __name__, template_folder=config.TEMPLATE_FOLDER, static_folder=config.STATIC_FOLDER
)
app.secret_key = config.FLASK_SECRET_KEY
app.config["REMEMBER_COOKIE_DURATION"] = datetime.timedelta(
    days=config.REMEMBER_COOKIE_DURATION
)

login.login_manager.init_app(app)

app.register_blueprint(actions.actions)
