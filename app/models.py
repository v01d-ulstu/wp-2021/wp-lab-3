import sqla_wrapper
import flask_login

import config


db = sqla_wrapper.SQLAlchemy(
    f"sqlite:///{config.DATABASE_PATH}?check_same_thread=False"
)


class User(db.Model, flask_login.UserMixin):
    __tablename__ = "users"

    id = db.Column(db.Integer, primary_key=True)
    login = db.Column(db.String, nullable=False, unique=True)
    password = db.Column(db.String, nullable=False)
    is_admin = db.Column(db.Boolean, nullable=False, default=False)

    posts = db.relationship("Post", backref="user", lazy=True)
    likes = db.relationship("Like", backref="user", lazy=True)


class Post(db.Model):
    __tablename__ = "posts"

    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String, nullable=False)
    text = db.Column(db.String, nullable=False)
    publish_date = db.Column(db.DateTime, nullable=False)
    hidden = db.Column(db.Boolean, nullable=False, default=False)

    user_id = db.Column(db.Integer, db.ForeignKey("users.id"), nullable=False)
    images = db.relationship("Image", backref="post", lazy=True)
    likes = db.relationship("Like", backref="post", lazy=True)

    def get_annotation(self, max_length=100):
        return (
            self.text
            if len(self.text) <= max_length
            else self.text[:max_length] + "..."
        )


class Image(db.Model):
    __tablename__ = "images"

    id = db.Column(db.Integer, primary_key=True)
    url = db.Column(db.String, nullable=False)

    post_id = db.Column(db.Integer, db.ForeignKey("posts.id"), nullable=False)


class Like(db.Model):
    __tablename__ = "likes"

    id = db.Column(db.Integer, primary_key=True)

    post_id = db.Column(db.Integer, db.ForeignKey("posts.id"))
    user_id = db.Column(db.Integer, db.ForeignKey("users.id"))

    @staticmethod
    def find_user_like(user_id: int, post_id: int):
        return (
            db.query(Like)
            .filter(Like.post_id == post_id, Like.user_id == user_id)
            .one_or_none()
        )
